<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\User;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $post = factory(App\Post::class, 3)->create();
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'wah apa neh?',
            'image' => 'img/foto11.jpg',
        ]);
        //
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Bye-bye, Have a good time',
            'image' => 'img/foto22.jpg',
        ]);

        // DB::table('posts')->insert([
        //     'user_id' => '5',
        //     'caption' => 'Hello, This is my third post!',
        //     'image' => 'img/foto3.jpg',
        // ]);
    }
}
