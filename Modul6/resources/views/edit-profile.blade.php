@extends('layouts.app')

@section('content')

<div class="container" style="padding-top: 20px;">


        <div class="container" style="padding: 0 180px 0 180px;">
            <h3>Edit Profile</h3>
            <form method="post" action="{{url('/home/editprofile/editprofil/'.Auth::user()->id)}}" enctype="multipart/form-data">

                 @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input name="title" type="text" class="form-control" id="title" placeholder="" value="{{ Auth::user()->title }}">
                </div>
                <div class="form-group">
                    <label for="Description">Description</label>
                    <input name="description" type="text" class="form-control" id="Description" value="{{ Auth::user()->description }}">
                </div>
                <div class="form-group">
                    <label for="Url">Url</label>
                    <input name="url" type="text" class="form-control" id="url" value="{{ Auth::user()->url }}">
                </div>
                <div class="form-group">
                    <label for="Avatar">Profile Image</label>
                    <input name="avatar" type="file" class="form-control-file" id="avatar" value="">
                </div>

                <input type="hidden" name="" value="{{ Auth::user()->id }}">

                <button type="submit" class="btn btn-primary">Save Profile</button>

            </form>
        </div>

</div>
@endsection
