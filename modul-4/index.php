<!DOCTYPE html>
<html lang="en">
<head>
<!-- required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <title>Home</title>

    <!-- bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <?php
    include(config.php);
    $query= "SELECT * FROM user_table";
    $select= mysqli_query($conn,$query);
    
    ?>
    
</head>
<body>
    <!-- NAVUGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <img href="index.php"src="EAD.png" width="10%" alt="">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="login.php">Masuk <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="register.php">Daftar</a>
      </li>
     
  </div>
</nav>
<!-- NAVIGATION BAR ENDED -->
    
    <section class="pricing py-5">
  <div class="container">
    <div class="row">
      <!-- Free Tier -->
      <div class="col-lg-4">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
            <h5 class="card-title text-muted text-uppercase text-center">Free</h5>
            <h6 class="card-price text-center">Free<span class="period">/Per-Bulan</span></h6>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fas fa-check"></i></span>Single User</li>
              </ul>
            <a href=login.php"" class="btn btn-block btn-primary text-uppercase">Buy</a>
          </div>
        </div>
      </div>
      <!-- Plus Tier -->
      <div class="col-lg-4">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
            <h5 class="card-title text-muted text-uppercase text-center">Basic</h5>
            <h6 class="card-price text-center">Rp.100.000<span class="period">/Per-Bulan</span></h6>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>5 Users</strong></li>
              </ul>
            <a href=login.php"" class="btn btn-block btn-primary text-uppercase">Buy</a>
          </div>
        </div>
      </div>
      <!-- Pro Tier -->
      <div class="col-lg-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title text-muted text-uppercase text-center">Pro</h5>
            <h6 class="card-price text-center">Rp.250.000<span class="period">/Per-Bulan</span></h6>
            <hr>
            <ul class="fa-ul">
              <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited Users</strong></li>
              </ul>
            <a href="login.php"" class="btn btn-block btn-primary text-uppercase">Buy</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


</body>
</html>